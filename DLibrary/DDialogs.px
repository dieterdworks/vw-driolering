{*
============================
 DDialogs
============================
	
 By Dieter Geerts @ DWorks.
 (c) 2013, July 14.
============================
*}

{*
FUNCTIONS/PROCEDURES
=================================================================================
DCreateLayout                 // Creates a dialog layout.
DCreateGroupBox               // Creates a group box control.
DCreateListBox                // Creates a list box control.
DCreateClassPullDownMenu      // Creates a class pull down menu control.
DCreatePushButton             // Creates a push button control.
DSetFirstLayoutItem           // Set first layout item.
DSetFirstGroupItem			  // Set first group item.
DSetRightItem                 // Set right item.
DSetBelowItem                 // Set below item.
DAddChoiceArray               // Adds all array items to the list.
DAddChoiceResourceList        // Adds all resource list items to the list.
DAddChoiceRecordFields        // Adds all record fields to the list.
DAddChoiceFiles               // Adds all files from a given directory.
DAddClassChoiceByPio          // Adds the proposed class 'by pio'.
DSelectChoiceByName           // Set selected item in choice list.
DSelectClassChoiceByName      // Set selected class in choice list.
DGetSelectedChoiceInfo        // Gets the choice info of the selected item.
DGetSelectedClassChoiceInfo   // Gets the class choice info of the selected item.
DRemoveChoiceAll              // Removes all choices.
DRemoveChoiceFiles            // Removes all files from a directoryType.
DSortChoice                   // Sorts the choice list.
DInsertImagePopupResourceList // Insert resource list in the image popup.
DSelectImagePopupByName       // Set selected item in image popup list.
=================================================================================
*}

{*///
	<group>Dialogs</group>
	<description>Creates a dialog layour.
	Will look for the language setting to localize
	the title and buttons. Help tekst is always on.
	By Dieter Geerts @ DWorks.</description>
///*}
FUNCTION DCreateLayout(subTitle : STRING) : LONGINT;
	VAR
		titleString  : STRING;
		okString     : STRING;
		cancelString : STRING;
		
	BEGIN
		IF (subTitle = '') THEN BEGIN
			titleString := Concat(pluginName, ' | ', DGetDWorksVersion);
		END ELSE BEGIN
			titleString := Concat(pluginName, ' - ', subTitle, ' | ', DGetDWorksVersion);
		END;
		
		IF (GetPluginString(sidLanguage) = 'NL') THEN BEGIN
			okString     := 'Ok';
			cancelString := 'Annuleer';
		END ELSE BEGIN
			okString     := 'Ok';
			cancelString := 'Cancel';
		END;
		
		DCreateLayout := CreateLayout(titleString, TRUE, okString, cancelString);
	END;

{*///
	<group>Dialogs</group>
	<description>Creates a group box control.
	By Dieter Geerts @ DWorks.</description>
///*}
FUNCTION DCreateGroupBox(dialogID : LONGINT; VAR controlID : LONGINT; name : STRING) : DCONTROL;
	BEGIN
		controlID := controlID + 1;
		CreateGroupBox(dialogID, controlID, name, TRUE);
		
		{// Setting structure parameters.}
		DCreateGroupBox.dialogID := dialogID;
		DCreateGroupBox.fieldID  := controlID;
		DCreateGroupBox.labelID  := 0;
	END;

{*///
	<group>Dialogs</group>
	<description>Creates a list box control.
	By Dieter Geerts @ DWorks.</description>
///*}
FUNCTION DCreateListBox(dialogID : LONGINT; VAR controlID : LONGINT; width, height : LONGINT) : DCONTROL;
	BEGIN
		controlID := controlID + 1;
		CreateListBox(dialogID, controlID, width, height);
		
		{// Setting structure parameters.}
		DCreateListBox.dialogID := dialogID;
		DCreateListBox.fieldID  := controlID;
		DCreateListBox.labelID  := 0;
	END;

{*///
	<group>Dialogs</group>
	<description>Creates a class pull down menu control.
	By Dieter Geerts @ DWorks.</description>
///*}
FUNCTION DCreateClassPullDownMenu(dialogID : LONGINT; VAR controlID : LONGINT; name : STRING; labelWidth, fieldWidth : LONGINT) : DCONTROL;
	BEGIN
		controlID := controlID + 1;
		CreateStaticText(dialogID, controlID, name, labelWidth);
		controlID := controlID + 1;
		CreateClassPullDownMenu(dialogID, controlID, fieldWidth);
		SetRightItem(dialogID, controlID - 1, controlID, 0, 0);
		
		{// Setting structure parameters.}
		DCreateClassPullDownMenu.dialogID := dialogID;
		DCreateClassPullDownMenu.fieldID  := controlID;
		DCreateClassPullDownMenu.labelID  := controlID - 1;
	END;

{*///
	<group>Dialogs</group>
	<description>Creates a push button control.
	By Dieter Geerts @ DWorks.</description>
///*}
FUNCTION DCreatePushButton(dialogID : LONGINT; VAR controlID : LONGINT; name : STRING) : DCONTROL;
	BEGIN
		controlID := controlID + 1;
		CreatePushButton(dialogID, controlID, name);

		{// Setting structure parameters.}
		DCreatePushButton.dialogID := dialogID;
		DCreatePushButton.fieldID  := controlID;
		DCreatePushButton.labelID  := 0;
	END;

{*///
	<group>Dialogs</group>
	<description>Set first layout item.
	By Dieter Geerts @ DWorks.</description>
///*}
PROCEDURE DSetFirstLayoutItem(control : DCONTROL);
	BEGIN
		IF (control.labelID > 0) THEN BEGIN
			SetFirstLayoutItem(control.dialogID, control.labelID);
		END ELSE BEGIN
			SetFirstLayoutItem(control.dialogID, control.fieldID);
		END;
	END;

{*///
	<group>Dialogs</group>
	<description>Set first group item.
	By Dieter Geerts @ DWorks.</description>
///*}
PROCEDURE DSetFirstGroupItem(groupControl, control : DCONTROL);
	BEGIN
		IF (control.labelID > 0) THEN BEGIN
			SetFirstGroupItem(groupControl.dialogID, groupControl.fieldID, control.labelID);
		END ELSE BEGIN
			SetFirstGroupItem(groupControl.dialogID, groupControl.fieldID, control.fieldID);
		END;
	END;
	
{*///
	<group>Dialogs</group>
	<description>Set right item.
	By Dieter Geerts @ DWorks.</description>
///*}
PROCEDURE DSetRightItem(baseControl, control : DCONTROL);
	BEGIN
		IF (control.labelID > 0) THEN BEGIN
			SetRightItem(baseControl.dialogID, baseControl.fieldID, control.labelID, 0, 0);
		END ELSE BEGIN
			SetRightItem(baseControl.dialogID, baseControl.fieldID, control.fieldID, 0, 0);
		END;
	END;	
	
{*///
	<group>Dialogs</group>
	<description>Set below item.
	By Dieter Geerts @ DWorks.</description>
///*}
PROCEDURE DSetBelowItem(baseControl, control : DCONTROL);
	BEGIN
		IF ((baseControl.labelID > 0) & (control.labelID > 0)) THEN BEGIN
			SetBelowItem(baseControl.dialogID, baseControl.labelID, control.labelID, 0, 0);
		END ELSE BEGIN
			SetBelowItem(baseControl.dialogID, baseControl.fieldID, control.fieldID, 0, 0);
		END;
	END;	
	
{*///
	<group>Dialogs</group>
	<description>Adds all array items to the PullDownMenu or ListBox.
	Will add the items in the order of the array.
	By Dieter Geerts @ DWorks.</description>
///*}
PROCEDURE DAddChoiceArray(dialogID, listID : LONGINT; items : DYNARRAY[] of STRING);
	VAR
		itemIndex : INTEGER;
		firstRow : INTEGER;
		lastRow : INTEGER;
		firstColumn : INTEGER;
		lastColumn : INTEGER;

	BEGIN
		GetArrayDimensions(items, firstRow, lastRow, firstColumn, lastColumn);
		FOR itemIndex := 1 TO lastRow DO BEGIN
			AddChoice(dialogID, listID, items[itemIndex], itemIndex - 1);
		END;
	END;

{*///
	<group>Dialogs</group>
	<description>Adds all resource list items to the PullDownMenu or ListBox.
	Will add the items in the order of the resource list.
	By Dieter Geerts @ DWorks.</description>
///*}
PROCEDURE DAddChoiceResourceList(dialogID, listID : LONGINT; resourceList : LONGINT);
	VAR
		resourceListArray : DYNARRAY[] of STRING;
		resourceListCount : LONGINT;
		resourceListIndex : LONGINT;
		
	BEGIN
		resourceListCount := ResourceListSize(resourceList);
		IF (resourceListCount > 0) THEN BEGIN
			ALLOCATE resourceListArray[1..resourceListCount];
			FOR resourceListIndex := 1 TO resourceListCount DO BEGIN
				resourceListArray[resourceListIndex] := GetNameFromResourceList(resourceList, resourceListIndex);
			END;
			DAddChoiceArray(dialogID, listID, resourceListArray);
		END;
	END;
	
{*///
	<group>Dialogs</group>
	<description>Adds all record fields to the PullDownMenu or ListBox.
	Will add the items in the order of the record.
	By Dieter Geerts @ DWorks.</description>
///*}
PROCEDURE DAddChoiceRecordFields(dialogID, listID : LONGINT; recordName : STRING);
	VAR
		recordHandle : HANDLE;
		fieldArray : DYNARRAY[] of STRING;
		fieldCount : INTEGER;
		fieldIndex : INTEGER;
		
	BEGIN
		recordHandle := GetObject(recordName);
		IF ((recordHandle <> NIL) AND (GetTypeN(recordHandle) = kObjTypeRecordDef)) THEN BEGIN
			fieldCount := NumFields(recordHandle);
			ALLOCATE fieldArray[1..fieldCount];
			FOR fieldIndex := 1 TO fieldCount DO BEGIN
				fieldArray[fieldIndex] := GetFldName(recordHandle, fieldIndex);
			END;
			DAddChoiceArray(dialogID, listID, fieldArray);
		END;
	END;
	
{*///
	<group>Dialogs</group>
	<description>Adds all files from the given directory to the PullDownMenu or ListBox.
	The directory must exist and be accesable. SearchOptions can be specified.
	The directoryType can be given and will be placed between brackets after the file name.
	This directoryType can be used as a reference when reading the choice from the list.
	By Dieter Geerts @ DWorks.</description>
///*}
PROCEDURE DAddChoiceFiles(dialogID, listID : LONGINT; directory, directoryType, searchOptions : STRING);
	VAR
		fileIndex : INTEGER;
		fileName : STRING;
		
	BEGIN
		{// Setting the directory reference.}
		IF (directoryType <> '') THEN directoryType := Concat(' (', directoryType, ')');
		
		{// Adding the files to the list.}
		fileIndex := 0;
		fileName := DGetNextFileInFolder(directory, searchOptions, fileIndex);
		WHILE (fileName <> '') DO BEGIN
			AddChoice(dialogID, listID, Concat(fileName, directoryType), 0);
			fileName := DGetNextFileInFolder(directory, searchOptions, fileIndex);
		END;
	END;

{*///
	<group>Dialogs</group>
	<description>Adds the proposed class 'by pio', which is <pluginName>.
	By Dieter Geerts @ DWorks.</description>
///*}
PROCEDURE DAddClassChoiceByPio(control : DCONTROL);
	BEGIN
		IF InsertPropClassOrLayerItem(control.dialogID, control.fieldID , Concat('<', pluginName, '>'), '') THEN ;
	END;

{*///
	<group>Dialogs</group>
	<description>There is no way in VS to select the choice item by name.
	So this will do it. It will loop through the choices until it finds the name.
	When nothing is found, this function will return FALSE.
	By Dieter Geerst @ DWorks.</description>
///*}
FUNCTION DSelectChoiceByName(control : DCONTROL; itemName : STRING) : BOOLEAN;
	VAR
		itemFound : BOOLEAN;
		itemIndex : LONGINT;
		itemCount : INTEGER;
		itemText  : STRING;

	BEGIN
		GetChoiceCount(control.dialogID, control.fieldID, itemCount);
		WHILE (NOT itemFound) & (itemIndex <= itemCount) DO BEGIN
			GetChoiceText(control.dialogID, control.fieldID, itemIndex, itemText);
			itemFound := (itemText = itemName);
			itemIndex := itemIndex + 1;
		END;
		IF itemFound THEN BEGIN
			SelectChoice(control.dialogID, control.fieldID, itemIndex - 1, TRUE);
			DSelectChoiceByName := TRUE;
		END;
	END;

{*///
	<group>Dialogs</group>
	<description>Selects the class choice item by name. In VS dialog,
	we need to add the <pluginName> proposed class manually and the string
	in the choice will be <pluginName>, while in the oip and as parameter value,
	this choice is ''. So we will transform these for the user.
	Another thing to note is that when the class name doesn't exists in the document,
	we will add it to the pulldown list, so that in the dialog it is displayed to the
	user to reflect the loaded setting and it can be selected.
	By Dieter Geerst @ DWorks.</description>
///*}
FUNCTION DSelectClassChoiceByName(control : DCONTROL; itemName : STRING) : BOOLEAN;
	VAR
		itemIndex: INTEGER;

	BEGIN
		IF (itemName = '') THEN itemName := Concat('<', pluginName, '>');
		DSelectClassChoiceByName := DSelectChoiceByName(control, itemName);
		IF NOT DSelectClassChoiceByName THEN BEGIN
    		itemIndex := InsertEnhanPullDownMenuItem(control.dialogID, control.fieldID, itemName, '');
    		DSelectClassChoiceByName := DSelectChoiceByName(control, itemName);
		END;
	END;

{*///
	<group>Dialogs</group>
	<description>Gets the choice info of the selected item.
	By Dieter Geerst @ DWorks.</description>
///*}
PROCEDURE DGetSelectedChoiceInfo(control : DCONTROL; VAR choiceIndex : INTEGER; VAR choiceString : STRING);
	BEGIN
		GetSelectedChoiceInfo(control.dialogID, control.fieldID, 0, choiceIndex, choiceString);
	END;

{*///
	<group>Dialogs</group>
	<description>Gets the class choice info of the selected item.
	By Dieter Geerst @ DWorks.</description>
///*}
PROCEDURE DGetSelectedClassChoiceInfo(control : DCONTROL; VAR choiceIndex : INTEGER; VAR choiceString : STRING);
	BEGIN
		DGetSelectedChoiceInfo(control, choiceIndex, choiceString);
		
		{// Transform the 'by class' choiceString.}
		IF (choiceString = Concat('<', pluginName, '>')) THEN choiceString := '';
	END;

{*///
	<group>Dialogs</group>
	<description>Removes all items from the PullDownMenu or ListBox.
	By Dieter Geerts @ DWorks.</description>
///*}
PROCEDURE DRemoveChoiceAll(dialogID, listID : LONGINT);
	VAR
		itemIndex : LONGINT;
		itemCount : INTEGER;
		itemText : STRING;
		
	BEGIN
		GetChoiceCount(dialogID, listID, itemCount);
		FOR itemIndex := itemCount - 1 DOWNTO 0 DO BEGIN
			RemoveChoice(dialogID, listID, itemIndex);
		END;
	END;
	
{*///
	<group>Dialogs</group>
	<description>Removes all files from the directoryType from the PullDownMenu or ListBox.
	It will remove all files from the directoryType.
	By Dieter Geerts @ DWorks.</description>
///*}
PROCEDURE DRemoveChoiceFiles(dialogID, listID : LONGINT; directoryType : STRING);
	VAR
		itemIndex : LONGINT;
		itemCount : INTEGER;
		itemText : STRING;
		
	BEGIN
		GetChoiceCount(dialogID, listID, itemCount);
		itemIndex := itemCount - 1;
		WHILE (itemIndex >= 0) DO BEGIN
			GetChoiceText(dialogID, listID, itemIndex, itemText);
			IF (Pos(Concat(' (', directoryType, ')'), itemText) > 0) THEN BEGIN
				RemoveChoice(dialogID, listID, itemIndex);
			END;
			itemIndex := itemIndex - 1;
		END;
	END;

{*///
	<group>Dialogs</group>
	<description>Sorts the list alphabetically.
	By Dieter Geerts @ DWorks.</description>
///*}
PROCEDURE DSortChoice(control : DCONTROL);
	VAR
		itemIndex : LONGINT;
		itemCount : INTEGER;
		itemText : STRING;
		items : DYNARRAY of STRING;
		selectedIndex : INTEGER;
		selectedText : STRING;
		
	BEGIN
		{// Get the selected item.}
		GetSelectedChoiceInfo(control.dialogID, control.fieldID, 0, selectedIndex, selectedText);
		
		{// Get all items in an array for easy sorting.}
		GetChoiceCount(control.dialogID, control.fieldID, itemCount);
		ALLOCATE items[1..itemCount];
		WHILE (itemIndex < itemCount) DO BEGIN
			GetChoiceText(control.dialogID, control.fieldID, itemIndex, itemText);
			items[itemIndex + 1] := itemText;
			itemIndex := itemIndex + 1;
		END;
		SortArray(items, itemCount, 0);
		
		{// Remove all items in the list and add the sorted array.}
		DRemoveChoiceAll(control.dialogID, control.fieldID);
		DAddChoiceArray(control.dialogID, control.fieldID, items);
		
		{// Set the selected item.}
		IF (selectedIndex > -1) THEN BEGIN
			IF NOT DSelectChoiceByName(control, selectedText) THEN BEGIN
				SelectChoice(control.dialogID, control.fieldID, 0, TRUE);
			END;
		END;
	END;

{*///
	<group>Dialogs</group>
	<description>VS only let us insert one resource into an image popup.
	This procedure will insert a whole list for easy use.</description>
///*}
PROCEDURE DInsertImagePopupResourceList(dialogID, controlID, resourceList : LONGINT);
	VAR
		resourceIndex : LONGINT;
		popupItemIndex : LONGINT;
	
	BEGIN
		FOR resourceIndex := 1 TO ResourceListSize(resourceList) DO BEGIN
			popupItemIndex := InsertImagePopupResource(dialogID, controlID, resourceList, resourceIndex);
		END;	
	END;
	
{*///
	<group>Dialogs</group>
	<description>There is no way in VS to select the image popup item by name.
	When nothing is found, this function will return FALSE.</description>
///*}
FUNCTION DSelectImagePopupByName(dialogID, controlID : LONGINT; itemName : STRING) : BOOLEAN;
	VAR
		itemIndex : LONGINT;
		
	BEGIN
		itemIndex := GetImagePopupObjectItemIndex(dialogID, controlID, itemName);
		IF itemIndex <> 0 THEN BEGIN
			SetImagePopupSelectedItem(dialogID, controlID, itemIndex);
			DSelectImagePopupByName := TRUE;
		END;
	END;
	